from typing import List

from fastapi import APIRouter, Depends

from ..models.messages import (
    Message,
    MessageCreate,
    MessageUpdate
)
from ..services.message import MessageService

router = APIRouter(
    # prefix='/clients',
    tags=['Messages']
)


@router.get('/send_message/{message_id}')
def send_message(message_id: int,
                 service: MessageService = Depends()
                 ):
    """
    Отправка письма на внешний сервис
    """
    return service.send_message(message_id=message_id)
