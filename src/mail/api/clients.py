from typing import List

from fastapi import APIRouter, Depends

from ..models.clients import (
    Client,
    ClientCreate, ClientUpdate
)
from ..services.Clients import ClientService

router = APIRouter(
    # prefix='/clients',
    tags=['Client']
)


@router.post('/create_client', response_model=Client)
def create_client(client_data: ClientCreate,
                  service: ClientService = Depends()
                  ):
    """
    Создание клиента в базе
    """
    return service.create(client_data=client_data)


@router.put('/update_client/{client_id}', response_model=Client)
def update_client(client_id: int,
                  client_data: ClientUpdate,
                  service: ClientService = Depends()
                  ):
    """
    Обновление данных клиента в базе
    Нужно вписать все поля, либо они будут None
    Частичное обновление модели не смог реализовать , не понимаю ,где ошибка
    """
    return service.update(client_id=client_id, client_data=client_data)


@router.get('/clients', response_model=List[Client])
def get_clients(
        service: ClientService = Depends()
):
    """
    Все клиенты в базе
    """
    return service.get_list()

@router.delete('/{client_id}')
def delete_client(
        client_id: int,
        service: ClientService = Depends()
        ):
    """
    Удаление клиента по его ID в базе данных
   """
    return service.delete(client_id=client_id)

