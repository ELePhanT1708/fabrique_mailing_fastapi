from typing import List

from fastapi import APIRouter, Request, Depends
from starlette.responses import HTMLResponse, FileResponse
from src.mail.services.web_ui import UIService
from fastapi.templating import Jinja2Templates

templates = Jinja2Templates(directory="templates")
# from .api import router

router = APIRouter(
    # prefix='/clients',
    tags=['Visualizing']
)


@router.get("/", response_class=HTMLResponse)
async def read_item(request: Request,
                    service: UIService = Depends()):
    mailing_lists = service.get_lists()
    return templates.TemplateResponse("mailing_list.html", {"request": request,
                                                            "title": 'Manager',
                                                            "lists": mailing_lists})


@router.get("/messages", response_class=HTMLResponse)
async def read_item(request: Request,
                    service: UIService = Depends()):
    messages = service.get_messages()
    return templates.TemplateResponse("messages.html", {"request": request,
                                                        "title": 'Manager',
                                                        "messages": messages})


@router.get("/mailing_list/{mailing_list_id}", response_class=HTMLResponse)
async def read_item(mailing_list_id: int,
                    request: Request,
                    service: UIService = Depends()):
    mailing_list = service.get_list_by_id(mailing_list_id)
    included_clients = service.get_clients_by_list(mailing_list)
    statistic_data = service.collect_statistic_for_mailing_list(mailing_list.id)
    return templates.TemplateResponse("mailing_list_view.html", {"request": request,
                                                                 "title": 'Manager',
                                                                 "list": mailing_list,
                                                                 "clients": included_clients,
                                                                 "statistic": statistic_data})


favicon_path = 'static/favicon.ico'


@router.get('/static/favicon.ico', include_in_schema=False)
async def favicon():
    return FileResponse(favicon_path)
