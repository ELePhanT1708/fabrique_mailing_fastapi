from typing import List

from fastapi import APIRouter, Depends
from starlette.responses import JSONResponse

from ..models.mailings import (
    Mail,
    MailCreate,
    MailUpdate
)
from ..services.mailing_lists import MailService

router = APIRouter(
    # prefix='/clients',
    tags=['Mailing list']
)


@router.post('/create_mailing_list', response_model=Mail)
def create_mailing_list(mailing_list_data: MailCreate,
                        service: MailService = Depends()
                        ):
    """
    Создание рассылки в базе данных
    """
    return service.create(mailing_list_data=mailing_list_data)


@router.put('/update_mailing_list/{mailing_list_id}', response_model=Mail)
def update_mailing_list(mailing_list_id: int,
                        mailing_list_data: MailUpdate,
                        service: MailService = Depends()
                        ):
    """
    Обновление данных рассылки в базе
    Нужно вписать все поля, либо они будут None
    """
    return service.update(mailing_list_id=mailing_list_id, mailing_list_data=mailing_list_data)


@router.get('/mailing_lists', response_model=List[Mail])
def get_mailing_lists(
        service: MailService = Depends()
):
    """
    Все рассылки в базе данных
    """
    return service.get_list()


@router.delete('/{mailing_list_id}')
def delete_mailing_list(
        mailing_list_id: int,
        service: MailService = Depends()
):
    """
    Удаление рассылки по его ID в базе данных
   """
    return service.delete(mailing_list_id=mailing_list_id)


@router.get('/mailing_lists_report/{mailing_list_id}')
def list_statistic(
        mailing_list_id: int,
        service: MailService = Depends()
):
    """
    Стаитистика по рассылке
    """
    return service.collect_statistic(mailing_list_id)
