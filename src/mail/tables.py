import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


class Client(Base):
    __tablename__ = 'clients'
    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    phone = sa.Column(sa.BIGINT, unique=True)
    code_phone = sa.Column(sa.Text)
    tag = sa.Column(sa.Text)
    timezone = sa.Column(sa.Integer)


class Mailing_list(Base):
    __tablename__ = 'mailing_lists'
    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    data_start = sa.Column(sa.DATETIME)
    data_end = sa.Column(sa.DATETIME)
    msg_body = sa.Column(sa.Text)
    filter = sa.Column(sa.String)


class Message(Base):
    __tablename__ = 'messages'
    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    send_time = sa.Column(sa.DATETIME)
    status = sa.Column(sa.Text)
    client_id = sa.Column(sa.Integer, sa.ForeignKey('clients.id'))
    list_id = sa.Column(sa.Integer, sa.ForeignKey('mailing_lists.id'))

    # relations
    client = relationship('Client', foreign_keys='Message.client_id')
    mail_list = relationship('Mailing_list', foreign_keys='Message.list_id')
