from datetime import datetime

from pydantic import BaseModel
from typing import Optional
from enum import Enum


class BaseMessage(BaseModel):
    send_time: datetime
    status: str
    client_id: int
    list_id: int


class MessageCreate(BaseMessage):
    pass


class MessageUpdate(BaseMessage):
    pass


class Message(BaseMessage):
    id: int

    class Config:
        orm_mode = True
