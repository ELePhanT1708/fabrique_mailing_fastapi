from datetime import datetime

from pydantic import BaseModel
from typing import Optional
from enum import Enum


class BaseMail(BaseModel):
    data_start: datetime
    data_end: datetime
    msg_body: str
    filter: Optional[str]


class MailCreate(BaseMail):
    pass


class MailUpdate(BaseMail):
    pass


class Mail(BaseMail):
    id: int

    class Config:
        orm_mode = True
