from pydantic import BaseModel
from typing import Optional
from enum import Enum


class BaseClient(BaseModel):
    phone: int
    code_phone: str
    tag: str
    timezone: Optional[int]


class ClientCreate(BaseClient):
    pass


class ClientUpdate(BaseClient):
    pass


class Client(BaseClient):
    id: int

    class Config:
        orm_mode = True
