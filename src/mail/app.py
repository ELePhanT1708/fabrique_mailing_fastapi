
from fastapi import FastAPI

from src.mail import tables
from src.mail.api.clients import router as client_router
from src.mail.api.mailing_lists import router as mailing_list_router
from src.mail.api.message import router as message_router
from src.mail.api.web_ui import router as visualizer_router
from src.mail.db import engine
from fastapi.staticfiles import StaticFiles

tables.Base.metadata.create_all(bind=engine)

app = FastAPI(
    title='Mailing_app',
    description='Сервис для управления рассылками',
    version='1.0.0',
)


def configure_static(app: FastAPI):
    app.mount("/static", StaticFiles(directory="static"), name="static")


app.include_router(client_router)
app.include_router(mailing_list_router)
app.include_router(message_router)
app.include_router(visualizer_router)

