from datetime import datetime, timedelta
from typing import List, Optional
from threading import Timer
import pandas as pd
import requests
from fastapi import Depends, HTTPException, status, Response
from sqlalchemy.orm import Session
from sqlalchemy import func

from .. import tables
from ..db import get_session
from ..models.mailings import MailCreate, MailUpdate, Mail
from ..models.messages import Message


class MailService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def _get(self, mailing_list_id: int):
        mailing_list = self.session.query(tables.Mailing_list) \
            .filter_by(id=mailing_list_id) \
            .first()
        if not mailing_list:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
        return mailing_list


    def create(self, mailing_list_data: MailCreate) -> tables.Mailing_list:
        mailing_list = tables.Mailing_list(**mailing_list_data.dict())
        self.session.add(mailing_list)
        self.session.commit()
        time_now = datetime.utcnow()
        if mailing_list.data_start < time_now < mailing_list.data_end:
            self.create_messages_for_mailing_list()
        if time_now < mailing_list.data_start:
            delta = mailing_list.data_start - time_now
            seconds = delta//pd.Timedelta(seconds=1)
            task = Timer(seconds, self.create_messages_for_mailing_list)
            task.start()
        return mailing_list

    def create_messages_for_mailing_list(self):
        mailing_list = self.session.query(tables.Mailing_list).order_by(tables.Mailing_list.id.desc()).first()
        list_of_clients = self.session.query(tables.Client) \
            .filter(tables.Client.tag.contains(mailing_list.filter)).all()
        for client in list_of_clients:
            message = tables.Message(send_time=datetime.utcnow(),
                                     status='IN_PROGRESS',
                                     client_id=client.id,
                                     list_id=mailing_list.id)
            self.session.add(message)
            self.session.commit()
            self.send_message(message)
        pass

    def send_message(self, message):
        api_url = f'https://probe.fbrq.cloud/v1/send/{message.id}'
        mailing_list_entity = self.session.query(tables.Mailing_list) \
            .filter_by(id=message.list_id).first()
        msg_body = mailing_list_entity.msg_body
        client = self.session.query(tables.Client) \
            .filter_by(id=message.client_id).first()
        phone = client.code_phone + str(client.phone)
        data = {
            'id': message.id,
            'phone': phone,
            'text': msg_body
        }
        my_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9' \
                   '.eyJleHAiOjE3MDMzMzQxMjgsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBz' \
                   'Oi8vdC5tZS9FTGVQaGFuVF8xNzA4In0.KuYquNiFlB32AiNm0n4Ok1oJVnb9CLAe_aKFozPm-EQ'
        message.status = 'IN_PROGRESS'
        self.session.commit()
        response = requests.post(api_url, headers={'Authorization': 'Bearer ' + my_token}, json=data,
                                 timeout=2)
        if response.json()['message'] == 'OK':
            message.status = 'SUCCESS'
            self.session.commit()
            return response.json()
        else:
            message.status = "FAILED"
            self.session.commit()
            return response.json()

    def update(self, mailing_list_id: int, mailing_list_data: MailUpdate) -> tables.Mailing_list:
        stored_mailing_list_data = self._get(mailing_list_id)
        for key, value in mailing_list_data:
            setattr(stored_mailing_list_data, key, value)
        self.session.commit()
        return stored_mailing_list_data

    def get_list(self) -> List[tables.Mailing_list]:
        mailing_lists = self.session.query(tables.Mailing_list).all()
        return mailing_lists

    def delete(self, mailing_list_id: int) -> None:
        mailing_list = self._get(mailing_list_id=mailing_list_id)
        self.session.delete(mailing_list)
        self.session.commit()
        return Response(status_code=status.HTTP_204_NO_CONTENT)
