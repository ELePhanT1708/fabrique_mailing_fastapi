from http.client import HTTPException
import requests
from fastapi import Depends
from starlette import status

from src.mail import tables
from src.mail.db import get_session, Session
from src.mail.models.messages import MessageCreate, Message


class MessageService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def _get(self, message_id: int):
        message = self.session.query(tables.Message) \
            .filter_by(id=message_id) \
            .first()
        if not message:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
        return message






