from http.client import HTTPException

from fastapi import Depends, Request
from starlette import status

from src.mail import tables
from src.mail.db import get_session, Session
from src.mail.models.mailings import Mail


class UIService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def get_lists(self):
        mailing_lists = self.session.query(tables.Mailing_list).all()
        return mailing_lists

    def get_list_by_id(self, mailing_list_id: int):
        mailing_list = self.session.query(tables.Mailing_list).filter_by(id=mailing_list_id).one()
        return mailing_list

    def get_clients_by_list(self, mailing_list: Mail):
        messages = self.session.query(tables.Message).filter_by(list_id=mailing_list.id).all()
        clients = [self.session.query(tables.Client).filter_by(id=message.client_id).one() for message in messages]
        return clients

        # messages = self.session.query(tables.Message).filter_by(list_id=mailing_list_id).one()
        return mailing_list

    def get_messages(self):
        messages = self.session.query(tables.Message).all()
        return messages

    def collect_statistic_for_mailing_list(self, mailing_list_id: int):
        messages = self.session.query(tables.Message).filter_by(list_id=mailing_list_id).all()
        msg_all = len(messages)
        msg_success = 0
        msg_failed = 0
        msg_in_progress = 0
        clients = []
        for msg in messages:
            if msg.status == 'SUCCESS':
                msg_success += 1
            if msg.status == 'FAILED':
                msg_failed += 1
            if msg.status == 'IN_PROGRESS':
                msg_in_progress += 1
        result = {'all': msg_all,
                  'in_progress': msg_in_progress,
                  'success': msg_success,
                  'failed': msg_failed,
                  }
        return result
