from typing import List, Optional

from fastapi import Depends, HTTPException, status, Response
from sqlalchemy.orm import Session

from .. import tables
from ..db import get_session
from ..models.clients import Client, ClientCreate, ClientUpdate


class ClientService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def _get(self, client_id: int):
        client = self.session.query(tables.Client) \
            .filter_by(id=client_id) \
            .first()
        if not client:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
        return client

    def create(self, client_data: ClientCreate) -> tables.Client:
        client = tables.Client(**client_data.dict())
        self.session.add(client)
        self.session.commit()
        return client

    def update(self, client_id: int, client_data: ClientUpdate) -> tables.Client:
        stored_client_data = self._get(client_id)
        for key, value in client_data:
            setattr(stored_client_data, key, value)
        self.session.commit()
        return stored_client_data

    def get_list(self) -> List[tables.Client]:
        clients = self.session.query(tables.Client).all()
        return clients

    def delete(self, client_id: int) -> None:
        client = self._get(client_id=client_id)
        self.session.delete(client)
        self.session.commit()
        return Response(status_code=status.HTTP_204_NO_CONTENT)



