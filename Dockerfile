FROM python:3.10

ENV PYTHONUNBUFFERED=1

WORKDIR /app/fastapi

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

EXPOSE 8001

WORKDIR /app/fastapi/

CMD ["uvicorn", "src.mail.app:app", "--port", "8001", "--reload"]